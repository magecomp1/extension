<?php
namespace Magecomp\S3Amazon\Plugin\Adminhtml\Iframe;

use Magento\Framework\App\Filesystem\DirectoryList;

class Show extends \Magento\Swatches\Controller\Adminhtml\Iframe\Show
{ 
    protected $s3;
    protected $_helperdatas3;

    public function __construct(
        \Magecomp\S3Amazon\Helper\Data $helper,
        \Magecomp\S3Amazon\Model\Core\File\Storage\S3 $s3,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Swatches\Helper\Media $swatchHelper,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\Catalog\Model\Product\Media\Config $config,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory)
    {
        $this->_helperdatas3 = $helper;
        $this->_s3 = $s3;
        $this->swatchHelper = $swatchHelper;
        $this->adapterFactory = $adapterFactory;
        $this->config = $config;
        $this->filesystem = $filesystem;
        $this->uploaderFactory = $uploaderFactory;
        parent::__construct($context,$swatchHelper,$adapterFactory,$config,$filesystem,$uploaderFactory);
    }

    public function aroundExecute(\Magento\Swatches\Controller\Adminhtml\Iframe\Show $subject, callable $proceed)
    {
        try {
            $uploader = $this->uploaderFactory->create(['fileId' => 'datafile']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
            $imageAdapter = $this->adapterFactory->create();
            $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
            $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);

            $config = $this->config;
            $result = $uploader->save($mediaDirectory->getAbsolutePath($config->getBaseTmpMediaPath()));
            unset($result['path']);
        
            $this->_eventManager->dispatch(
                'swatch_gallery_upload_image_after',
                ['result' => $result, 'action' => $this]
            );

            unset($result['tmp_name']);

            $result['url'] = $this->config->getTmpMediaUrl($result['file']);
            $result['file'] = $result['file'] . '.tmp';

            if($this->_helperdatas3->isS3Enabled()){
                $newFile = $this->swatchHelper->moveImageFromTmp('/'.$result['file']);
            }else{
                $newFile = $this->swatchHelper->moveImageFromTmp($result['file']);
            }

            $this->swatchHelper->generateSwatchVariations($newFile);
            $fileData = ['swatch_path' => $this->swatchHelper->getSwatchMediaUrl(), 'file_path' => $newFile];
            $this->getResponse()->setBody(json_encode($fileData));

            if($this->_helperdatas3->isS3Enabled()):
                $this->_s3->saveFile('attribute/swatch'.$newFile);
                $swatch_thumb=$this->swatchHelper->getFolderNameSize('swatch_thumb',null);
                $swatch_image=$this->swatchHelper->getFolderNameSize('swatch_image',null);
                $this->_s3->saveFile('attribute/swatch/swatch_thumb/'.$swatch_thumb.$newFile);
                $this->_s3->saveFile('attribute/swatch/swatch_image/'.$swatch_image.$newFile);
            endif;
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
            $this->getResponse()->setBody(json_encode($result));
        }
    }
}